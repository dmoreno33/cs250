#include "Image.hpp"

#include <iostream>
#include <string>
using namespace std;

int main()
{
    bool done = false;
    string filename;
    int filter;

    while ( !done )
    {
        cout << "---------------------" << endl;
        cout << "Work with image: ";
        getline( cin, filename );

        PpmImage image;
        image.LoadImage( filename );

        bool filterFound = false;

        while (!filterFound)
        {
            cout << "Which filter? (1-4) ";
            cin >> filter;

            switch (filter)
            {
            case 1:
                image.ApplyFilter1();
                filterFound = true;
                break;

            case 2:
                image.ApplyFilter2();
                filterFound = true;
                break;

            case 3:
                image.ApplyFilter3();
                filterFound = true;
                break;

            case 4:
                image.ApplyFilter4();
                filterFound = true;
                break;
            default:
                cout << "Not a valid value." << endl;
                break;
            }
        }
        

        cout << "Output filename: ";
        cin.ignore();
        getline( cin, filename );
        image.SaveImage( filename );
    }

    return 0;
}
