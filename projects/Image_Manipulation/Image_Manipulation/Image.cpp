#include "Image.hpp"

#include <fstream>
#include <iostream>
#include <exception>
#include <stdlib.h>
using namespace std;

PpmImage::PpmImage()
{
    m_pixels = new Pixel * [IMAGE_WIDTH];

    for (int i = 0; i < IMAGE_WIDTH; i++)
    {
        m_pixels[i] = new Pixel[IMAGE_HEIGHT];
    }
}

PpmImage::~PpmImage()
{
    for (int i = 0; i < IMAGE_WIDTH; i++)
    {
        delete[] m_pixels[i];
    }

    delete[] m_pixels;
}

void PpmImage::LoadImage( const string& filename )
{
    cout << "Loading image from \"" << filename << "\"...";

    ifstream input;
    input.open(filename, fstream::in);

    if (input.fail())
    {
        cout << "File not found/Filename typed incorrectly!" << endl;
        cout << "Make sure to type in the ending of the filenames! (.pmm)" << endl;
        exit(1);
    }

    string strBuffer;
    int intBuffer;

    getline(input, strBuffer); //Skip "P3"
    getline(input, strBuffer); //Skip comment
    input >> intBuffer; //Skip width
    input >> intBuffer; //Skip height
    input >> m_colorDepth; //Read the color depth

    int x = 0, y = 0;
    while (input >> m_pixels[x][y].r
                 >> m_pixels[x][y].g
                 >> m_pixels[x][y].b)
    {
        x++;
        if (x == IMAGE_WIDTH)
        {
            y++;
            x = 0;
        }
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::SaveImage( const string& filename )
{
    cout << "Saving image to \"" << filename << "\"...";

    ofstream output;
    output.open(filename, fstream::out);

    if (output.fail())
    {
        cout << "File could not save!" << endl;
        exit(1);
    }

    output << "P3" << endl;
    output << "# Comment" << endl;
    output << IMAGE_WIDTH << " " << IMAGE_HEIGHT << endl;
    output << m_colorDepth << endl;

    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
        for (int x = 0; x < IMAGE_WIDTH; x++)
        {
            output
                << m_pixels[x][y].r << endl
                << m_pixels[x][y].g << endl
                << m_pixels[x][y].b << endl;
        }
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter1()
{
    cout << "Applying filter 1...";

    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
        for (int x = 0; x < IMAGE_WIDTH; x++)
        {
            int y2 = IMAGE_HEIGHT - 1 - y;
            int x2 = IMAGE_WIDTH - 1 - x;

            m_pixels[x][y].r = m_pixels[x2][y2].r;
            m_pixels[x][y].g = m_pixels[x2][y2].g;
            m_pixels[x][y].b = m_pixels[x2][y2].b;
        }
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter2()
{
    cout << "Applying filter 2...";

    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
        for (int x = 0; x < IMAGE_WIDTH; x++)
        {
            int r = m_pixels[x][y].r;
            int g = m_pixels[x][y].g;
            int b = m_pixels[x][y].b;

            m_pixels[x][y].r = g;
            m_pixels[x][y].g = b;
            m_pixels[x][y].b = r;
        }
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter3()
{
    cout << "Applying filter 3...";

    int r = 0;
    int g = 0;
    int b = 0;

    for (int x = 0; x < IMAGE_WIDTH; x++)
    {
        for (int y = 0; y < IMAGE_HEIGHT; y++)
        {
            r++;
            g++;
            b++;

            m_pixels[x][y].r += r;
            m_pixels[x][y].g += g;
            m_pixels[x][y].b += b;
        }
        r = 0;
        g = 0;
        b = 0;
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter4()
{
    cout << "Applying filter 4...";

    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
        for (int x = 0; x < IMAGE_WIDTH; x++)
        {
            int GLITCH = rand() % 15 + 5;

            m_pixels[x][y].r -= GLITCH;
            m_pixels[x][y].g -= GLITCH;
            m_pixels[x][y].b -= GLITCH;
        }
    }

    cout << "SUCCESS" << endl;
}
