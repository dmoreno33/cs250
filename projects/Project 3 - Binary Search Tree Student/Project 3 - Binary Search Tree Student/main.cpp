#include "DataStructure/BinarySearchTreeTester.hpp"
#include "Utilities/Logger.hpp"

int main()
{
    Logger::Setup();

    BinarySearchTreeTester tester;
    tester.Start();

    Logger::Cleanup();

    return 0;
}
