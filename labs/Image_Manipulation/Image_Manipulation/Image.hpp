#ifndef _IMAGE_HPP
#define _IMAGE_HPP

#include <string>
using namespace std;

const int IMAGE_WIDTH = 300;
const int IMAGE_HEIGHT = 300;

struct Pixel
{
    int r, g, b;
};

class PpmImage
{
    public:
    void LoadImage( const string& filename );
    void SaveImage( const string& filename );

    void ApplyFilter1();
    void ApplyFilter2();

    private:
    int m_colorDepth;
    Pixel m_pixels[IMAGE_WIDTH][IMAGE_HEIGHT];
};

#endif
