#include "Image.hpp"

#include <fstream>
#include <iostream>
#include <exception>
using namespace std;

void PpmImage::LoadImage( const string& filename )
{
    cout << "Loading image from \"" << filename << "\"...";

    ifstream input;
    input.open(filename, fstream::in);

    if (input.fail())
    {
        throw invalid_argument("Filename typed wrong/File not found!");
    }

    string strBuffer;
    int intBuffer;

    getline(input, strBuffer); //Skip "P3"
    getline(input, strBuffer); //Skip comment
    input >> intBuffer; //Skip width
    input >> intBuffer; //Skip height
    input >> m_colorDepth; //Read the color depth

    int x = 0, y = 0;
    while (input >> m_pixels[x][y].r
        >> m_pixels[x][y].g
        >> m_pixels[x][y].b)
    {
        x++;
        if (x == IMAGE_WIDTH)
        {
            y++;
            x = 0;
        }
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::SaveImage( const string& filename )
{
    cout << "Saving image to \"" << filename << "\"...";

    ofstream output;
    output.open(filename, fstream::in);

    if (output.fail())
    {
        throw invalid_argument("File could not save!");
    }

    output << "P3" << endl;
    output << "# Comment" << endl;
    output << IMAGE_WIDTH << ", " << IMAGE_HEIGHT << endl;
    output << m_colorDepth;

    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
        for (int x = 0; x < IMAGE_WIDTH; x++)
        {
            output
                << m_pixels[x][y].r << endl
                << m_pixels[x][y].g << endl
                << m_pixels[x][y].b << endl;
        }
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter1()
{
    cout << "Applying filter 1...";

    throw runtime_error( "Method not implemented!" );

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter2()
{
    cout << "Applying filter 2...";

    throw runtime_error( "Method not implemented!" );

    cout << "SUCCESS" << endl;
}
