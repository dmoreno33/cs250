#ifndef STUDENT_HPP
#define STUDENT_HPP

#include <string>
using namespace std;

struct Student
{
    Student() { fname = ""; lname = ""; schoolId = 0; schoolName = ""; }
    Student( string fname, string lname ) { this->fname = fname; this->lname = lname; schoolId = 0; schoolName = ""; }

    //! Student first name
    string fname;
    //! Student last name
    string lname;
    //! School ID (key) that the student is linked to
    int schoolId;
    //! School name (value)
    string schoolName;

    //! Output stream operator
    friend std::ostream& operator<< ( std::ostream& stream, const Student& student );
};

std::ostream& operator<< ( std::ostream& stream, const Student& student )
{
    stream << student.fname << " " << student.lname << " " << ", " << student.schoolId << " - " << student.schoolName;
    return stream;
}

#endif
