#include <cstdlib>
#include <vector>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
using namespace std;

int main()
{
    srand( time( NULL ) );

    map<int, string> schools;
    map<int, string> students;
    vector<int> schoolIds;
    
    string buffer, buffer2;
    
    ifstream schoolsFile( "schools.txt" );
    while ( getline( schoolsFile, buffer ) )
    {
        if ( buffer != "" )
        {
            int randId = ( rand() % 1000 ) + 100;
            while ( schools.find( randId ) != schools.end() )
            {
                randId = ( rand() % 1000 ) + 100;
            }
            schools[ randId ] = buffer;
            schoolIds.push_back( randId );
        }
    }

    cout << schools.size() << " schools loaded" << endl;

    ifstream studentFile( "students.txt" );
    while ( studentFile >> buffer >> buffer2 )
    {
        int randId = ( rand() % 10000 ) + 1000;
        while ( students.find( randId ) != students.end() )
        {
            randId = ( rand() % 10000 ) + 1000;
        }
        students[ randId ] = buffer + " " + buffer2;
    }

    cout << students.size() << " students loaded" << endl;

    ofstream output( "actions.txt" );
    for ( map<int, string>::iterator it = schools.begin(); it != schools.end(); it++ )
    {
        output << "ADD_SCHOOL\n" << it->second << "\nWITH_SCHOOL_ID\n" << it->first << "\n\n";
    }
    
    for ( map<int, string>::iterator it = students.begin(); it != students.end(); it++ )
    {
        output << "ADD_STUDENT\n" << it->second << "\nWITH_STUDENT_ID\n" << it->first << "\n\n";
    }
    
    for ( map<int, string>::iterator it = students.begin(); it != students.end(); it++ )
    {
        int randSchool = rand() % schoolIds.size();
        output << "PUT_STUDENT\n" << it->first << "\nIN_SCHOOL\n" << schoolIds[ randSchool ] << "\n\n";
    }
    
    return 0;
}
