#ifndef _FOLDER
#define _FOLDER

#include <vector>
using namespace std;

#include "FileSystemThing.hpp"
#include "File.hpp"

class Folder : public FileSystemThing
{
    public:
    Folder();
    Folder( string name );

    void Display();
    void Details();

    void AddFile( File* ptrFile );
    void AddFolder( Folder* ptrFolder );

    vector<Folder*>& GetFolders();
    vector<File*>& GetFiles();

    protected:
    vector<Folder*> m_childFolders;
    vector<File*> m_childFiles;

    friend class FileSystemManager;
};

#endif
