#include "FileSystemThing.hpp"

FileSystemThing::FileSystemThing() {}

FileSystemThing::FileSystemThing( string name, string type )
{
    SetName( name );
    m_type = type;
}

void    FileSystemThing::SetName( string name )
{
    m_name = name;
}

string  FileSystemThing::GetName()
{
    return m_name;
}

string FileSystemThing::GetType()
{
    return m_type;
}

void FileSystemThing::Display()
{
    cout << m_name;
}

void FileSystemThing::Details()
{
    cout << "Name:              " << m_name << endl;
    cout << "Type:              " << m_type << endl;
}
