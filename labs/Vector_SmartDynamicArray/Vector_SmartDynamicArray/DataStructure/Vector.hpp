#ifndef _VECTOR_HPP
#define _VECTOR_HPP

#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

class Vector
{
    private:
    /* Member Variables */
    //! A string pointer used to allocate memory for a dynamic array.
    string* m_data;

    //! Stores how many items have been added to the Vector; less than or equal to the m_arraySize.
    int m_itemCount;

    //! Stores how large the array currently is; update after Resize() is called.
    int m_arraySize;

    public:
    /* Member Functions */
    // Constructor and Destructor
    Vector();
    ~Vector();

    // Deal with end of vector
    void Push(const string& newItem);

    // Deal with middle of vector
    string Get( int index ) const;
    void Remove( int index );

    // Helper functions
    int Size() const;
    bool IsFull() const;
    bool IsEmpty() const;

    private:
    // Internal helper functions
    bool IsInvalidIndex( int index ) const;
    bool IsElementEmpty( int index );

    // Memory mangement
    void AllocateMemory( int newSize = 10 );
    void DeallocateMemory();
    void Resize();

    // Special function, since we haven't covered exceptions yet
    void Panic( string message ) const;
    void NotImplemented() const;

    friend class Tester;
};

// ******************************** Initialization functions * //

Vector::Vector()
{
    m_data = nullptr;
    m_itemCount = 0;
    m_arraySize = 0;
}

Vector::~Vector()
{
    DeallocateMemory();
}

void Vector::Push(const string& newItem)
{
    if (m_data == nullptr)
    {
        AllocateMemory();
    }
    else if (IsFull())
    {
        Resize();
    }

    int insertAt = -1;
    for (int i = 0; i < m_arraySize; i++)
    {
        if (m_data[i] == "")
        {
            insertAt = i;
            break;
        }
    }

    m_data[m_itemCount] = newItem;
    m_itemCount++;
}

// Deal with middle of vector
string Vector::Get(int index) const
{
    if (IsInvalidIndex(index))
    {
        Panic("Invalid Index");
    }

    return m_data[index];
}

void Vector::Remove(int index)
{
    if (IsInvalidIndex(index))
    {
        Panic("Invalid index");
    }
    else if (m_data[index] == "")
    {
        Panic("Element is already empty");
    }
    else
    {
        m_data[index] = "";
        m_itemCount--;
    }
}

// Helper functions
int Vector::Size() const
{
    return m_itemCount;
}

bool Vector::IsFull() const
{
    return (m_itemCount == m_arraySize);
}

bool Vector::IsEmpty() const
{
    return (m_itemCount == 0);
}

// Internal helper functions
bool Vector::IsInvalidIndex(int index) const
{
    return (index < 0 || index >= m_arraySize);
}

bool Vector::IsElementEmpty(int index)
{
    if ( IsInvalidIndex(index))
    {
        Panic("Invalid index");
    }

    return (m_data[index] == "");
}

// Memory mangement
void Vector::AllocateMemory(int newSize)
{
    if (m_data == nullptr)
    {
        m_data = new string[newSize];
        m_arraySize = newSize;
        m_itemCount = 0;
    }
    else
    {
        throw logic_error("Cannot allocate memory - pointer is already pointing to an address.");
    }
}

void Vector::DeallocateMemory()
{

    if (m_data != nullptr)
    {
        delete[] m_data;
        m_data = nullptr;
        m_itemCount = 0;
        m_arraySize = 0;
    }
}

void Vector::Resize()
{
    int newSize = m_arraySize + 10;
    string* newArr = new string[newSize];

    for (int i = 0; i < m_arraySize; i++)
    {
        newArr[i] = m_data[i];
    }

    delete[] m_data;

    m_data = newArr;

    m_arraySize = newSize;
}

// Special function, since we haven't covered exceptions yet
void Vector::Panic(string message) const 
{
    throw runtime_error(message);
}

void Vector::NotImplemented() const 
{
    throw runtime_error("Function not implemented yet!");
}

#endif
